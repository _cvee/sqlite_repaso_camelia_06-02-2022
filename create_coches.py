import sqlite3

conexion = sqlite3.connect("bd2.db")

try:
    conexion.execute("""
                        CREATE TABLE coches (
                            coche_id INTEGER PRIMARY KEY,
                            marca TEXT NOT NULL,
                            modelo TEXT NOT NULL,
                            potencia INTEGER NOT NULL,
                            km INTEGER NOT NULL,
                            prop_id INTEGER NOT NULL,
                            FOREIGN KEY (prop_id)
                            REFERENCES propietarios (prop_id)
                            ON DELETE CASCADE )


                        CREATE TABLE propietarios (
                            prop_id INTEGER,
                            nombre TEXT NOT NULL,
                            apellido TEXT NOT NULL,
                            PRIMARY KEY (prop_id)
                    """)
    print("Se creo la tabla de coches")
except sqlite3.OperationalError:
    print("La tabla de coches ya existe")
    
conexion.close()




