import sqlite3

conexion = sqlite3.connect("bd2.db")

try:
    conexion.execute("""
                        CREATE TABLE propietarios (
                            prop_id INTEGER,
                            nombre TEXT NOT NULL,
                            apellido TEXT NOT NULL,
                            PRIMARY KEY (prop_id)
                    """)
    print("Se creo la tabla de propetarios")
except sqlite3.OperationalError:
    print("La tabla de propetarios ya existe")
conexion.close()
